
# build

```
> node --version
v10.19.0

> npm -V
6.14.4
```

```
> npm install --force
> npm start // запустить сервер разработки
> npm run build // сделать локальную сборку
```

И отдать нжинксом все, что есть в `/build`

Пока ворнингов нет, но если билд не будет собираться из-за них, то в `package.json` поменять (30 строка):

```
"build": "react-scripts build",
```

на

```
"build": "CI=false; react-scripts build",
```

Для использования с версией node 18 и выше, `package.json` поменять строки

```
"start": "BROWSER=none PORT=3004 HTTPS=true react-scripts start",
"build": "react-scripts build",
```

на

```
"start": "NODE_OPTIONS=--openssl-legacy-provider BROWSER=none PORT=3004 HTTPS=true react-scripts start",
"build": "NODE_OPTIONS=--openssl-legacy-provider react-scripts build",
```