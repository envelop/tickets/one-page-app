
import React from 'react';
import Tippy from '@tippyjs/react';
import CopyToClipboard from 'react-copy-to-clipboard';
import {
	withRouter,
	match,
	Link
} from 'react-router-dom';
import {
	History,
	Location
} from 'history';
import {
	assetTypeToString,
	ERC20ContractParamsType,
	MetamaskAdapter,
	OriginalTokenType,
} from '../../models/BlockchainAdapter';

import {
	tokenPreviewClear,
} from '../../reducers';
import {
	compactString,
} from '../../models/_utils';

import icon_cannot_load from '../../static/pics/logo-mob.svg';
import icon_i_copy      from '../../static/pics/i-copy.svg';
import icon_i_action    from '../../static/pics/i-action.svg';
import icon_i_link      from '../../static/pics/icons/i-link.svg';
import icon_i_download  from '../../static/pics/icons/i-download.svg';

import { withTranslation, Trans } from "react-i18next";

import TransferPopup from '../TransferPopup';
import ApprovePopup from '../ApprovePopup';

import { requestTokenURIUpdate } from '../../models/APIService/apiservice';

import BigNumber from 'bignumber.js';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

type TokenPreviewPageProps = {
	store                 : any,
	metamaskAdapter       : MetamaskAdapter,
	token                 : OriginalTokenType;
	t                     : any,
	match                 : match;
	location              : Location,
	history               : History,
}
type TokenPreviewPageState = {
	chainId        : number,
	userAddress    : string,
	decimalsNative : number,
	iconNative     : string,
	symbolNative   : string,
	explorerBaseUrl: string,
	EIPPrefix      : string,

	techToken            : ERC20ContractParamsType,
	erc20CollateralTokens: Array<ERC20ContractParamsType>,
	erc20OtherTechTokens : Array<ERC20ContractParamsType>,
	transferAllowances   : Array<{ wrapperAddress: string, transferModelAddress: string, erc20TokenAddress: string, allowance: BigNumber }>,

	approvePopup: boolean,
	transferPopup: boolean,

	showJson  : boolean,
	copiedHint: undefined | { id: string, icon: string },

	menuOpened      : boolean,
	cursorOnCardMenu: boolean,
	tokensListOpened: boolean,

	tokenURIRequested: boolean,
}

class TokenPreviewPage extends React.Component<TokenPreviewPageProps, TokenPreviewPageState> {

	store            : any;
	metamaskAdapter  : MetamaskAdapter;
	unsubscribe!     : Function;
	t                : any;
	menuBlockRef     : React.RefObject<HTMLInputElement>;
	copiedHintTimer  : number;
	copiedHintTimeout: number;

	constructor(props: TokenPreviewPageProps) {
		super(props);

		this.store             = props.store;
		this.metamaskAdapter   = props.metamaskAdapter;
		this.t                 = props.t;
		this.menuBlockRef      = React.createRef();
		this.copiedHintTimer   = 0;
		this.copiedHintTimeout = 2; // s

		this.state = {
			chainId        : this.store.getState().metamaskAdapter.chainId,
			userAddress    : this.store.getState().account.address,
			decimalsNative : this.store.getState().metamaskAdapter.networkTokenDecimals,
			symbolNative   : this.store.getState().metamaskAdapter.networkTokenTicket,
			iconNative     : this.store.getState().metamaskAdapter.networkTokenIcon,
			explorerBaseUrl: this.store.getState().metamaskAdapter.explorerBaseUrl,
			EIPPrefix      : this.store.getState().metamaskAdapter.EIPPrefix,

			techToken            : this.store.getState().erc20TechTokenParams,
			erc20CollateralTokens: this.store.getState().erc20CollateralTokens,
			erc20OtherTechTokens : this.store.getState().erc20OtherTechTokens,
			transferAllowances   : this.store.getState().transferModelAllowances,

			approvePopup: false,
			transferPopup: false,

			showJson  : false,
			copiedHint: undefined,

			menuOpened           : false,
			cursorOnCardMenu     : false,
			tokensListOpened     : false,

			tokenURIRequested    : false,
		}
	}

	componentDidMount() {
		this.unsubscribe = this.store.subscribe(() => {

			this.setState({
				chainId        : this.store.getState().metamaskAdapter.chainId,
				userAddress    : this.store.getState().account.address,

				decimalsNative : this.store.getState().metamaskAdapter.networkTokenDecimals,
				symbolNative   : this.store.getState().metamaskAdapter.networkTokenTicket,
				iconNative     : this.store.getState().metamaskAdapter.networkTokenIcon,
				explorerBaseUrl: this.store.getState().metamaskAdapter.explorerBaseUrl,
				EIPPrefix        : this.store.getState().metamaskAdapter.EIPPrefix,

				techToken            : this.store.getState().erc20TechTokenParams,
				erc20CollateralTokens: this.store.getState().erc20CollateralTokens,
				erc20OtherTechTokens : this.store.getState().erc20OtherTechTokens,
				transferAllowances   : this.store.getState().transferModelAllowances,
			});
		});
 	}
	componentWillUnmount() {
		this.unsubscribe();
		this.store.dispatch(tokenPreviewClear());
	}

	getTokenMedia() {
		if ( this.props.token && this.props.token.image === undefined ) {
			return (
				<div className="inner">
					<div className="default">
						<img src={ icon_cannot_load } alt="" />
						<span><Trans i18nKey="Loading NON-FUNGIBLE TOKEN Preview" components={[<br />]} /></span>
					</div>
				</div>
			)
		}
		if ( this.props.token && this.props.token.image === '' ) {
			return (
				<div className="inner">
					<div className="default">
						<img src={ icon_cannot_load } alt="" />
						<span><Trans i18nKey="Cannot load NON-FUNGIBLE TOKEN Preview" components={[<br />]} /></span>
					</div>
				</div>
			)
		}

		return (
			<div className="inner">
				<video className="img" src={ this.props.token ? this.props.token.image : '' } poster={ this.props.token ? this.props.token.image : '' } autoPlay={ true } muted={ true } loop={ true } />
			</div>
		)
	}

	getCopiedHint(token: OriginalTokenType, icon: string) {
		let foundHint = false;
		if ( this.state.copiedHint ) {
		 	foundHint = ( this.state.copiedHint.id === `${token.contractAddress}${token.tokenId}` ) && ( this.state.copiedHint.icon === icon );
		}
		return (<span className="btn-action-info" style={{display: foundHint ? 'block' : 'none' }}>Copied</span>)
	}
	getMenuCopyLinkButton() {
		if ( !this.props.token ) { return null; }

		return (
			<li>
				<CopyToClipboard
					text={ `${window.location.origin}/token/${this.state.chainId}/${this.props.token.contractAddress}/${this.props.token.tokenId}` }
					onCopy={() => {
						if ( !this.props.token ) { return; }
						this.setState({
							copiedHint: { id: `${this.props.token.contractAddress}${this.props.token.tokenId}`, icon: 'tokenlinkimg' }
						});
						clearTimeout(this.copiedHintTimer);
						this.copiedHintTimer = window.setTimeout(() => { this.setState({
							copiedHint: undefined
						}); }, this.copiedHintTimeout*1000);
					}}
				>
					<button>
						Copy NFT URL
					</button>
				</CopyToClipboard>
			</li>
		)
	}
	getMenuTransferButton() {
		if ( this.props.token.owner && this.props.token.owner.toLowerCase() !== this.state.userAddress.toLowerCase() ) { return null; }
		if ( this.props.token.amount && this.props.token.amount.lte(0)                                               ) { return null; }
		return (
			<li>
				<button onClick={() => { this.setState({ transferPopup: true }) }}>
					Transfer
				</button>
			</li>
		)
	}
	getMenuApproveButton() {
		if ( this.props.token.owner && this.props.token.owner.toLowerCase() !== this.state.userAddress.toLowerCase() ) { return null; }
		if ( this.props.token.amount && this.props.token.amount.lte(0)                                               ) { return null; }

		return (
			<li>
				<button onClick={() => { this.setState({ approvePopup: true }) }}>
					Approve
				</button>
			</li>
		)
	}
	getMenuUpdateURIButton() {
		if ( this.state.tokenURIRequested ) { return (
			<li><button>Request sent</button></li>
		) }

		return (
			<li>
				<button
					onClick={() => {
						this.setState({ tokenURIRequested: true });
						requestTokenURIUpdate({
							chainId: this.state.chainId,
							contractAddress: this.props.token.contractAddress || '',
							tokenId: this.props.token.tokenId || '',
						})
					}}
				>
					Request token data update
				</button>
			</li>
		)
	}
	openCardMenu = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				if ( !this.menuBlockRef.current ) { return; }
				if ( e.path && e.path.includes(this.menuBlockRef.current) ) { return; }
				this.closeCardMenu();
			};
		}, 100);
		this.setState({ menuOpened: true });
	}
	closeCardMenu = () => {
		setTimeout(() => {
			if ( this.state.cursorOnCardMenu ) { return; }
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = null;
			this.setState({ menuOpened: false });
		}, 100);
	}
	getTokenMenu() {
		return (
			<div
				className="w-card__action"
				ref={ this.menuBlockRef }
			>
				<button
					className="btn-action"
					onMouseEnter={ this.openCardMenu }
					onMouseLeave={ this.closeCardMenu }
					onClick={ this.openCardMenu }
				>
					<img src={ icon_i_action } alt="" />
				</button>
				{
					this.state.menuOpened ?
					(
						<ul
							className="list-action"
							onMouseEnter={() => {
								this.openCardMenu();
								this.setState({ cursorOnCardMenu: true })
							}}
							onMouseLeave={() => {
								this.closeCardMenu();
								this.setState({ cursorOnCardMenu: false })
							}}
						>
							{ this.getMenuCopyLinkButton() }
							{ this.getMenuTransferButton() }
							{ this.getMenuApproveButton() }
							{ this.getMenuUpdateURIButton() }
						</ul>
					): null
				}
			</div>
		)
	}
	getTokenHeaderBlock() {
		return (
			<div className="w-card__meta">
				<div>

					<CopyToClipboard
						text={ this.props.token.contractAddress || '0x0000000000000000000000000000000000000000' }
						onCopy={() => {
							this.setState({
								copiedHint: { id: `${this.props.token.contractAddress}${this.props.token.tokenId}`, icon: 'address' }
							});
							clearTimeout(this.copiedHintTimer);
							this.copiedHintTimer = window.setTimeout(() => { this.setState({
								copiedHint: undefined
							}); }, this.copiedHintTimeout*1000);
						}}
					>
						<button className="btn-copy">
							<Tippy
								content={ this.props.token.contractAddress }
								appendTo={ document.getElementsByClassName("wrapper")[0] }
								trigger='mouseenter'
								interactive={ false }
								arrow={ false }
								maxWidth={ 512 }
							>
								<span>
									<span className="title">Address</span>
									{ compactString(this.props.token.contractAddress || '0x0000000000000000000000000000000000000000') }
								</span>
							</Tippy>
							<img src={icon_i_copy} alt="" />
							{ this.getCopiedHint(this.props.token, 'address') }
						</button>
					</CopyToClipboard>

					<CopyToClipboard
						text={ this.props.token.tokenId || '0' }
						onCopy={() => {
							this.setState({
								copiedHint: { id: `${this.props.token.contractAddress}${this.props.token.tokenId}`, icon: 'id' }
							});
							clearTimeout(this.copiedHintTimer);
							this.copiedHintTimer = window.setTimeout(() => { this.setState({
								copiedHint: undefined
							}); }, this.copiedHintTimeout*1000);
						}}
					>
						<button className="btn-copy" data-tippy-content="39677766365">
							<Tippy
								content={ this.props.token.tokenId }
								appendTo={ document.getElementsByClassName("wrapper")[0] }
								trigger='mouseenter'
								interactive={ false }
								arrow={ false }
								maxWidth={ 512 }
							>
								<span>
									<span className="title">ID</span>
									{ compactString(this.props.token.tokenId || '0x0000000000000000000000000000000000000000') }
								</span>
							</Tippy>

							<img src={icon_i_copy} alt="" />
							{ this.getCopiedHint(this.props.token, 'id') }
						</button>
					</CopyToClipboard>

				</div>
				{ this.getTokenMenu() }
			</div>
		)
	}
	getTokenUrl() {
		if ( !this.props.token ) { return ''; }
		if ( this.props.token.tokenUrlRaw ) { return `Token URI: ${this.props.token.tokenUrlRaw}`; }
		if ( this.props.token.tokenUrl ) { return `Token URI: ${this.props.token.tokenUrl}`; }
	}
	getJsonBlock() {
		if ( !this.state.showJson ) { return null; }
		if ( !this.props.token ) { return null; }
		if ( !this.props.token.tokenUrlRawJSON ) { return null; }

		let jsonParsed = 'Cannot load JSON';
		try {
			jsonParsed = JSON.stringify(JSON.parse(this.props.token.tokenUrlRawJSON), null, 4)
		} catch(e) {
			console.log('Cannot fetch tokenURL', e)
		}

		return (
			<div className="json-code">
				<div className="scroll-dark-bg">
					<pre>
						{
							this.getTokenUrl()
						}
						{ '\n\n' }
						{
							this.props.token && this.props.token.tokenUrlRawJSON ?
								jsonParsed
								: 'Cannot load JSON'
						}
					</pre>
				</div>
			</div>
		)
	}
	getWrapBtn() {
		if (
			( this.props.token.owner && this.props.token.owner.toLowerCase() === this.state.userAddress.toLowerCase() ) ||
			( this.props.token.amount && this.props.token.amount.gt(0) )
		)
		return (
			<div className="w-card__btn">
				<Link
					to={`/wrap/${this.state.chainId}/${this.props.token.contractAddress}/${this.props.token.tokenId}`}
					className="btn"
				>
					{ this.t('Wrap token') }
				</Link>
			</div>
		)
	}
	render() {

		return (
			<React.Fragment>
			<main className="s-main">
			<div className="container">
			<div className="w-card">
			<div className="bg">
				{ this.getTokenHeaderBlock() }
				<div className="w-card__token">
					<button className="btn-token-link">
						<img src={ icon_i_link } alt="" />
					</button>
					{ this.getTokenMedia() }
				</div>
				<div className="w-card__param with-padding">
					<div className="w-card__json">
						<div className="row">
							<div className="col-12 col-sm mb-3 mb-sm-0">
								<div className="name">{ this.props.token.name }</div>
								<div className="mb-1"><span className="text-muted">Type:</span> { assetTypeToString(this.props.token.assetType, this.state.EIPPrefix) }</div>
								{ this.props.token.amount ? <div className="mb-1"><span className="text-muted">Balance:</span> { this.props.token.amount.toString() }</div> : null }
								{ this.props.token.totalSupply ? <div className="mb-1"><span className="text-muted">Total supply:</span> { this.props.token.totalSupply.toString() }</div> : null }
							</div>
							<div className="col-12 col-sm-auto">
								<div className="btns-group">
									<div className="btn-group">
										<button
											className={`btn btn-sm btn-gray ${ this.state.showJson ? 'active' : '' }`}
											onClick={() => {
												this.setState({ showJson: !this.state.showJson });
											}}
										>
											{ !this.state.showJson ? 'View raw JSON' : 'Hide raw JSON' }
										</button>
									</div>
									<div className="btn-group">
										<a
											className="btn btn-sm btn-gray"
											target="_blank" rel="noopener noreferrer"
											href={ this.props.token.tokenUrl }
										>
											<img src={ icon_i_download } alt="" />
										</a>
									</div>
								</div>
							</div>
						</div>
						{ this.getJsonBlock() }
					</div>
					{ this.getWrapBtn() }
				</div>
			</div>
			</div>
			</div>
			</main>
			{
				this.state.transferPopup && this.props.token ?
				(
					<TransferPopup
						store={ this.store }
						metamaskAdapter={ this.metamaskAdapter }
						wrappedToken={ undefined }
						originalToken={ this.props.token }
						history={ this.props.history }
						closePopup={()=>{
							this.setState({ transferPopup: false })
						}}
					/>
				)
				: null
			}
			{
				this.state.approvePopup && this.props.token ?
				(
					<ApprovePopup
						store={ this.store }
						metamaskAdapter={ this.metamaskAdapter }
						wrappedToken={ undefined }
						originalToken={ this.props.token }
						history={ this.props.history }
						closePopup={()=>{
							this.setState({ approvePopup: false })
						}}
					/>
				)
				: null
			}
			</React.Fragment>
		)
	}
}

export default withTranslation("translations")(withRouter(TokenPreviewPage));