
import MetamaskAdapter        from './metamaskadapter';

import WrapperContract        from './wrappercontract';
import CheckerContract        from './checkercontract';
import CrossingDispatcher     from './crossingDispatcher';
import SAFTDispatcher         from './saftDispatcher';
import SubscriptionDispatcher from './subscriptionDispatcher';
import
	WNFTStorageContract,
	{
		calcTokenStats,
		sumCollaterals,
		fillCollateralsImages,
	}
from './wnftstoragecontract';
import NftMinterContract     from './nftmintercontract';

import ERC20Contract, {
	getERC20CollateralTokenFromStore,
} from './erc20contract';

import type { ChainParamsType }         from './metamaskadapter';
import type { ERC20ContractParamsType } from './erc20contract';

import type {
	_Asset,
	_AssetItem,
	_INData,
	WrapTransactionArgs,
	CollateralItem,
	_Royalty,
	Royalty,
	RoyaltyInput,
	_Lock,
	Lock,
	_Fee,
	Fee,
	Rules,
	OriginalTokenType,
	WrappedTokenType,
	SAFTRecipientItem,
	SAFTSubscriptionType,
	SAFTPayOption,
	SAFTTariff,
} from './_types';
import {
	_AssetType,
	LockType,
	encodeCollaterals,
	decodeCollaterals,
	encodeRoyalties,
	decodeRoyalties,
	getLockType,
	encodeLocks,
	decodeLocks,
	encodeFees,
	decodeFees,
	encodeRules,
	decodeRules,
	encodeINData,
	encodeWrapArguments,
	getNativeCollateral,
	assetTypeToString,
	originalToWrapped,
} from './_types';
import {
	getERC721Token,
	loadERC721TokenAll,
	saveERC721Token,
	removeERC721Token,
	transferERC721Token,
	mintToken,
	checkApprovalERC721Token,
}  from './erc721contract';
import type {
	WrappedTokensStatType,
} from './wrappercontract';

export {
	_AssetType,
	LockType,
	MetamaskAdapter,
	ERC20Contract,
	WrapperContract,
	CheckerContract,
	getERC721Token,
	loadERC721TokenAll,
	saveERC721Token,
	removeERC721Token,
	transferERC721Token,
	mintToken,
	WNFTStorageContract,
	NftMinterContract,
	checkApprovalERC721Token,

	CrossingDispatcher,
	SAFTDispatcher,
	SubscriptionDispatcher,

	encodeCollaterals,
	decodeCollaterals,
	encodeRoyalties,
	decodeRoyalties,
	getLockType,
	encodeLocks,
	decodeLocks,
	encodeFees,
	decodeFees,
	encodeRules,
	decodeRules,
	encodeINData,
	encodeWrapArguments,
	getNativeCollateral,

	getERC20CollateralTokenFromStore,

	assetTypeToString,

	calcTokenStats,
	sumCollaterals,
	fillCollateralsImages,

	originalToWrapped,
};

export type {
	_Asset,
	_AssetItem,
	_INData,
	_Fee,
	_Lock,
	_Royalty,
	Royalty,
	RoyaltyInput,
	Lock,
	Fee,
	WrapTransactionArgs,
	OriginalTokenType,

	ChainParamsType,
	ERC20ContractParamsType,
	WrappedTokenType,
	WrappedTokensStatType,
	CollateralItem,
	Rules,

	SAFTRecipientItem,
	SAFTSubscriptionType,
	SAFTPayOption,
	SAFTTariff,
}